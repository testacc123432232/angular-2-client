const express = require('express');
const path = require('path');
const db = require('./server/db');
const bodyParser = require('body-parser');

var expressApp = express();
var staticRoot = __dirname;

expressApp.set('port', (process.env.PORT || 3000));
expressApp.use(express.static(staticRoot));
expressApp.use(bodyParser.json());       // to support JSON-encoded bodies
expressApp.use(bodyParser.urlencoded({   // to support URL-encoded bodies
  extended: true
}));

expressApp.get('/', function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
});
expressApp.get('/home', function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
});
expressApp.get('/folder', function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
});
expressApp.get('/search/*', function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
});
expressApp.get('/page-details/*', function (req, res) {
  res.sendFile(path.join(__dirname,'index.html'))
});

/* api create new folder */
expressApp.post('/api/folder', function (req, res) {
  res.status(201).json(db.createFolder(req.body.title));
});

/* api get all folders */
expressApp.get('/api/folder', function (req, res) {
  res.json(db.getAllFolders(req.params.id));
});

/* api get folder by :id */
expressApp.get('/api/folder/:id', function (req, res) {
  res.status(200).json(db.getFolder(req.params.id));
});

/* api update folder by :id */
expressApp.put('/api/folder', function (req, res) {
  db.updateFolders(req.body.folders);
  res.status(200).json({success: 'message'});
});

/* api delete folder by :id */
expressApp.delete('/api/folder/:id/delete', function (req, res) {
  res.status(200).send(db.removeFolder(req.params.id));
});

/* api get folder pages by :id */
expressApp.get('/api/folder/:id/pages', function (req, res) {
  res.status(200).send(db.getFolderPages(req.params.id));
});

/* api get all pages */
expressApp.get('/api/page', function (req, res) {
  res.json(db.getAllPages(req.params.id));
});

/* api get page by :id */
expressApp.get('/api/page/:id', function (req, res) {
  res.status(200).json(db.getPage(req.params.id));
});

/* api get page by :term */
expressApp.get('/api/page/search/:term', function (req, res) {
  res.status(200).json(db.getPageByTitle(req.params.term));
});

/* api get page by :id */
expressApp.put('/api/page/:id', function (req, res) {
  res.status(200).json(db.updatePage(req.params.id, req.body.page));
});

expressApp.listen(expressApp.get('port'), function () {
  console.log('app running on port', expressApp.get('port'));
});