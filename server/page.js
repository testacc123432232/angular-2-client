"use strict";

var dbHelper = require('./db-helper');

var page = {

  getAllPages: function () {
    var pages = dbHelper.getFileData('pages.json');

    return pages || {};
  },

  getPage: function (pageId) {
    var pages = dbHelper.getFileData('pages.json');
    var page = dbHelper.getItemById(pages, pageId);

    return page || {};
  },

  getPageByTitle: function (title) {
    var pages = dbHelper.getFileData('pages.json');
    var resultPages = dbHelper.getItemByTitle(pages, title);

    return resultPages || {};
  },

  updatePage: function (id, page) {
    var pages = dbHelper.getFileData('pages.json');
    pages[id - 1] = page;
    dbHelper.setFileData('pages.json', pages);

    return {};
  }
};

module.exports = page;