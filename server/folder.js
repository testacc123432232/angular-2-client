"use strict";

var dbHelper = require('./db-helper');

function createFolder(title) {
  var folders = dbHelper.getFileData('folders.json');
  var newId = dbHelper.getNewId(folders);
  var newFolder = {'id': newId, 'title': title, 'items': []};

  folders.push(newFolder);

  dbHelper.setFileData('folders.json', folders);
}

function getAllFolders() {
  var folders = dbHelper.getFileData('folders.json');

  return folders || {};
}

function getFolder(folderId) {
  var folders = dbHelper.getFileData('folders.json');
  var folder = dbHelper.getItemById(folders, folderId);

  return folder || {};
}

function updateFolders(folders) {
  dbHelper.setFileData('folders.json', folders);
}

function removeFolder(folderId) {
  console.log('Remove folder number ' + folderId);
}

function getFolderPages(folderId) {
  var allPages = dbHelper.getFileData('pages.json');
  var resultPages = [];

  var folders = dbHelper.getFileData('folders.json');
  var folder = dbHelper.getItemById(folders, folderId);

  folder.items.forEach(function (item) {
    resultPages.push(dbHelper.getItemById(allPages, item));
  });

  return resultPages || {};
}

exports.createFolder = createFolder;
exports.getAllFolders = getAllFolders;
exports.getFolder = getFolder;
exports.updateFolders = updateFolders;
exports.removeFolder = removeFolder;
exports.getFolderPages = getFolderPages;
