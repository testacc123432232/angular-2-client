var folder = require('./folder');
var page = require('./page');

exports.createFolder = folder.createFolder;
exports.getFolder = folder.getFolder;
exports.getAllFolders = folder.getAllFolders;
exports.updateFolders = folder.updateFolders;
exports.removeFolder = folder.removeFolder;
exports.getFolderPages = folder.getFolderPages;

exports.getAllPages = page.getAllPages;
exports.getPage = page.getPage;
exports.getPageByTitle = page.getPageByTitle;
exports.updatePage = page.updatePage;
