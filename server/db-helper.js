"use strict";

var fs = require('fs');

function getFileData(fileName) {
  var fileData = fs.readFileSync('server/db/' + fileName);

  return JSON.parse(fileData);
}

function setFileData(fileName, data) {
  data = JSON.stringify(data);
  fs.writeFileSync('server/db/' + fileName, data);
}

function getItemById(data, id) {
  return data.find(function (item) {
    return item.id === +id;
  });
}

function getItemByTitle(data, title) {
  return data.filter(function (item) {
    if (~item.title.indexOf(title)) {
      return true;
    }
  });
}

function getNewId(data) {
  return (data.length + 1);
}

exports.getFileData = getFileData;
exports.setFileData = setFileData;
exports.getItemById = getItemById;
exports.getItemByTitle = getItemByTitle;
exports.getNewId = getNewId;
