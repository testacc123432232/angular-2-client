import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PageComponent }  from './page/page.component';
import { FolderComponent }  from './folder/folder.component';
import { PageDetailsComponent }  from './page-details/page-details.component';
import { PageSearchComponent }  from './page-search/page-search.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: PageComponent
  },
  {
    path: 'page-details/:id',
    component: PageDetailsComponent
  },
  {
    path: 'folder',
    component: FolderComponent
  },
  {
    path: 'search/:term',
    component: PageSearchComponent
  }
];
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class RoutesModule {}