import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PageService } from '../share/page.service';
import { Page } from '../page/page';

@Component({
  moduleId: module.id,
  selector: 'page-search',
  templateUrl: 'page-search.component.html',
})
export class PageSearchComponent implements OnInit {
  private title: string;
  private searchTerm: string;
  private searchResults: Page;

  /**
   * Class constructor
   *
   * @param pageService {PageService}
   * @param route {ActivatedRoute}
   * @param location {Location}
   */
  constructor(private pageService: PageService, private route: ActivatedRoute, private location: Location) {
    this.title = 'Search Page';
  }

  ngOnInit() {
    this.route.params.subscribe(param => {
      this.searchTerm = param['term'];
      this.pageService.getSearchResults(this.searchTerm).then(searchResults => this.searchResults = searchResults);
    });
  }

  public goBack(): void {
    this.location.back();
  }
}
