/**
 * Class Folder
 */
export class Folder {
  id: number;
  title: string;
  items: Array<number>;
  summary: string;

  constructor(title:string) {
    this.title = title;
  }
}