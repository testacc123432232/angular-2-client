import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { FolderService } from '../share/folder.service';
import { Folder } from './folder';
import { Page } from '../page/page';

@Component({
  moduleId: module.id,
  selector: 'my-folder',
  templateUrl: 'folder.component.html',
  providers: [FolderService]
})
export class FolderComponent implements OnInit {
  private title: string;
  private folders: Array<Folder>;
  private pages: Array<Page>;

  /**
   * Class Constructor
   *
   * @param folderService {FolderService}
   * @param location {Location}
   */
  constructor(private folderService: FolderService, private location: Location) {
    this.title = 'Folder Page'
  }

  ngOnInit() {
    this.getFolders();
  }

  public createFolder(title: string) {
    this.folders.push(new Folder(title));
    this.folderService.createFolder(title);
  }

  public goBack(): void {
    this.location.back();
  }

  public getPages(folderId: number) {
    this.folderService.getPages(folderId).then(pages => {
      this.pages = pages;
    });
  }

  private getFolders() {
    this.folderService.getFolders().then(folders => {
      this.folders = folders;
    });
  }
}
