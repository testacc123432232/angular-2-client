import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { ModalModule } from "ng2-modal";

import { AppComponent } from './app.component';
import { PageComponent } from './page/page.component';
import { FolderComponent } from './folder/folder.component';
import { PageDetailsComponent } from './page-details/page-details.component';
import { PageSearchComponent } from './page-search/page-search.component';
import { PageService } from './share/page.service';
import { FolderService } from './share/folder.service';
import { RoutesModule } from './routes.module';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RoutesModule,
    HttpModule,
    ModalModule
  ],
  declarations: [
    AppComponent,
    PageComponent,
    PageDetailsComponent,
    PageSearchComponent,
    FolderComponent
  ],
  providers: [PageService, FolderService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
