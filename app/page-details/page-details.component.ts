import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { PageService } from '../share/page.service';
import { FolderService } from '../share/folder.service';
import { Folder } from '../folder/folder';
import { Page } from '../page/page';
import { Modal, ModalModule } from 'ng2-modal';

@Component({
  moduleId: module.id,
  selector: 'page-details',
  templateUrl: 'page-details.component.html',
})
export class PageDetailsComponent implements OnInit {
  private title: string = '';
  private pageId: number;
  private page: Page;
  private folders: Array <Folder>;
  private selectFolder: Folder;
  @ViewChild('pageDetailsModal') pageDetailsModal: Modal;

  /**
   * Class constructor
   *
   * @param pageService {PageService}
   * @param folderService {FolderService}
   * @param route {ActivatedRoute}
   * @param location {Location}
   */
  constructor(private pageService: PageService,
              private folderService: FolderService,
              private route: ActivatedRoute,
              private location: Location) {
    this.title = 'Page Details Component'
  }

  ngOnInit() {
    this.getPageDetails();
  }

  public savePage() {
    this.pageService.savePageDetails(this.page).then(body => {
      if (body) {
        this.changeSelectFolder();
        this.folderService.saveFolders(this.folders);
        this.pageDetailsModal.close();
      }
    });
  }

  public goBack(): void {
    this.location.back();
  }

  private getPageDetails() {
    this.route.params.subscribe(param => {
      this.pageId = param['id'];
      this.pageService.getPageDetails(this.pageId).then(page => {
        this.page = page;
      });
      this.getFolders();
    });
  }

  private getFolders() {
    this.folderService.getFolders().then(folders => {
      this.folders = folders;
      this.selectFolder = this.getSelectFolder();
    });
  }

  private getSelectFolder() {
    let pageId = this.pageId;
    let folders = this.folders.filter(function (folder: Folder) {
      if (~folder.items.indexOf(+pageId)) {

        return true;
      }
    });

    return folders[0] || this.folders[0];
  }

  private changeSelectFolder() {
    let pageId = this.pageId;
    let selectFolder = this.selectFolder;

    this.folders.forEach(function (folder: Folder) {
      let deleteItem = folder.items.indexOf(+pageId);

      if (~deleteItem) {
        folder.items.splice(deleteItem, 1);
      }

      if (folder === selectFolder) {
        folder.items.push(+pageId);
      }
    });
  }
}
