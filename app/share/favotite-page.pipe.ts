import { Pipe, PipeTransform } from '@angular/core';

import { Page } from '../page/page';

@Pipe({ name: 'favoritePages' })
export class FavoritePagesPipe implements PipeTransform {
  transform(allPages: Page[]) {
    return allPages.filter(page => page.f1);
  }
}