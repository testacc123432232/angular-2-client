import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { BaseShareService } from './base-share.service';
import { Folder } from '../folder/folder';
import { Page } from '../page/page';

@Injectable()
export class FolderService extends BaseShareService {

  /**
   * Class constructor
   *
   * @param http {Http}
   */
  constructor(protected http: Http) {
    super(http);
    this.url = 'api/folder/';
  }

  public getFolders(): Promise<Array<Folder>> {
    return this.http.get(`${this.url}`, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  public createFolder(title: string) {
    return this.http.post(`${this.url}`, {'title': title}, {headers: this.getHeaders()})
      .toPromise()
      .catch(this.handleError);
  }

  public saveFolders(folders: Array<Folder>) {
    return this.http.put(`${this.url}`, {folders}, {headers: this.getHeaders()})
      .toPromise()
      .catch(this.handleError);
  }

  public getPages(folderId: number): Promise<Array<Page>> {
    return this.http.get(`${this.url}${folderId}/pages`, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  protected mapData(res: Response) {
    let body = res.json();

    return body || {};
  }
}
