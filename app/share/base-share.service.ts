import { Injectable } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export abstract class BaseShareService {
  protected url: string = '';

  /**
   * Class constructor
   *
   * @param http {Http}
   */
  constructor(protected http: Http) {
  }

  protected abstract mapData(res: Response): any;

  protected handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }

  protected getHeaders() {
    let headers = new Headers();
    headers.append('Accept', 'application/json');

    return headers;
  }
}
