import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import { BaseShareService } from './base-share.service';
import { Page } from '../page/page';

@Injectable()
export class PageService extends BaseShareService {

  /**
   * Class constructor
   *
   * @param http {Http}
   */
  constructor(protected http: Http) {
    super(http);
    this.url = 'api/page/';
  }

  public getPages(): Promise<Array<Page>> {
    return this.http.get(`${this.url}`, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  public getPageDetails(id: number): Promise<Page> {
    return this.http.get(`${this.url}${id}`, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  public savePageDetails(page: Page) {
    return this.http.put(`${this.url}${page.id}`, {page}, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  public getSearchResults(searchTerm: string): Promise<Page> {
    return this.http.get(`${this.url}search/${searchTerm}`, {headers: this.getHeaders()})
      .toPromise()
      .then(this.mapData)
      .catch(this.handleError);
  }

  protected mapData(res: Response) {
    let body = res.json();

    return body || {};
  }
}
