import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PageService } from '../share/page.service';
import { Page } from './page';

@Component({
  moduleId: module.id,
  selector: 'my-page',
  templateUrl: 'page.component.html',
  providers: [PageService]
})
export class PageComponent implements OnInit {
  private title: string;
  private pages: Array<Page>;

  /**
   * Class constructor
   *
   * @param pageService
   * @param router
   */
  constructor(private pageService: PageService, private router: Router) {
    this.title = 'Home Page'
  }

  ngOnInit() {
    this.getPages();
  }

  private getPages(): void {
    this.pageService.getPages().then(pages => this.pages = pages);
  }

  private goToSearchPage(term: any): void {
    let link = ['/search', term];
    this.router.navigate(link);
  }
}
