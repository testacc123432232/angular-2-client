/**
 * Page class
 */
export class Page {
  id: number;
  title: string;
  url: string;
  summary: string;
  f1: boolean;
  f2: boolean;
}